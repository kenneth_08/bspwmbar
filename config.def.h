/* See LICENSE file for copyright and license details. */

#ifndef BSPWMBAR_CONFIG_H_
#define BSPWMBAR_CONFIG_H_

#include "bspwmbar.h"

/* intel */
#define THERMAL_PATH "/sys/class/thermal/thermal_zone0/temp"
/* k10temp */
/* #define THERMAL_PATH "/sys/class/hwmon/hwmon1/temp1_input" */

/* max length of monitor output name and bspwm desktop name */
#define NAME_MAXSZ  32
/* max length of active window title */
#define TITLE_MAXSZ 50
/* set window height */
#define BAR_HEIGHT  24

/* set font pattern for find fonts, see fonts-conf(5) */
const char *fontname = "MonoSpace:size=10";  // "sans-serif:size=10";


/*
 * color settings by index of color map
 */
#define WHITE "#e5e5e5" /* white */
#define BLACK "#222222" /* black */
#define GRAY "#7f7f7f" /* light-gray */
#define DARK_GRAY "#555555" /* gray */
#define LIGHT_GREEN "#449f3d" /* light green (success) */
#define GREEN "#2f8419" /* green (Oke) */
#define ORANGE "#f5a70a" /* orange (Warning) */
#define RED "#ed5456" /* red (Critical) */
#define BLUE "#1793d1" /* blue (Arch logo color) */

const char *BGCOLOR = BLACK;  // Bar background color
const char *FGCOLOR = WHITE;  // Bar foreground color
const char *ALTBGCOLOR = DARK_GRAY;  // BarGraph (cpu & mem) background color
const char *ALTFGCOLOR = GRAY;  // BarGraph (cpu & mem) foreground color
const char *LOGOCOLOR = BLUE;  // Color of the logo

/*
 * Module definition
 */

/* modules on the left */
module_t left_modules[] = {
	{ /* Arch logo */
		.text = {
			.func = text,
			//.label = "",
			.label = "Arch",
			.fg = "#1793d1",
		},
	},
	{ /* bspwm desktop state */
		.desk = {
			.func = desktops,
			.active = "",
			.inactive = "",
		},
	},
	{ /* active window title */
		.title = {
			.func = windowtitle,
			.maxlen   = TITLE_MAXSZ,
			.ellipsis = "…",
		},
	},
};

/* modules on the right */
module_t right_modules[] = {
	{ /* system tray */
		.tray = {
			.func = systray,
			.iconsize = 16,
		},
	},
	{ /* cpu usage */
		.cpu = {
			.func = cpugraph,
			.prefix = " "
		},
	},
	{ /* cpu temperature */
		.thermal = {
			.func = thermal,
			.sensor = THERMAL_PATH,
			.prefix = " ",
			.suffix = "℃",
		},
	},
	{ /* memory usage */
		.mem = {
			.func = memgraph,
			.prefix = " "
		},
	},
	{ /* used space of root file system */
		.fs = {
			.func = filesystem,
			.mountpoint = "/",
			.prefix = " ",
			.suffix = "％",
		},
	},
	{ /* wireless network */
		.wireless_network = {
			.func = wireless_network,
            .network_card = "wlp1s0"
		},
	},
	{ /* backlight/brightness */
		.backlight = {
			.func = brightness,
            .display = "intel_backlight"
		},
	},
	{ /* master playback volume */
		.vol = {
			.func = volume,
			.handler = volume_ev,
			.suffix = "％",
			.muted = "婢",
			.unmuted = "墳",
		},
	},
	{ /* battery status */
		.battery = {
			.func = battery,
            .battery_id = "BAT1"
		},
	},
	{ /* clock */
		.date = {
			.func = datetime,
			.prefix = "",
			.format = "%d-%m-%Y  %H:%M:%S",
		},
	},
};

#endif
